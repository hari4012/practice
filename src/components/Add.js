import React, { Component } from 'react'

class Add extends Component {
    constructor(props) {
      super(props)
    
      this.state = {
        fnum:'',
        snum:'',
        tnum:0
      }
    }

    fnumHandler = (e)=>
    {
        this.setState({
            fnum:e.target.value
        })
    }

    snumHandler = (e)=>{
        this.setState({
            snum:e.target.value
        })
    }
    add(){
        this.setState({
            
            tnum:parseInt(this.state.fnum)+parseInt(this.state.snum)
        })
    }
  render() {
    return (
      <div>
        <input value={this.state.fum} onChange = {this.fnumHandler} />
        <input value={this.state.snum} onChange = {this.snumHandler} />
        <button onClick={()=>this.add()}>Add</button>
        <p>{this.state.tnum}</p>
      </div>
    )
  }
}

export default Add