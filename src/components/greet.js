import React from 'react'

function Greet(props) {
  return (
    <div>
        <h1>Hi {props.name} this is me!! and this is my {props.id} </h1>
        <p>{props.children}</p>
       
  </div>
  )
}

export default Greet
